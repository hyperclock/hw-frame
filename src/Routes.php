<?php declare(strict_types = 1);

return [
    ['GET', '/', ['HWFrame\Controllers\Homepage', 'show']],
];
